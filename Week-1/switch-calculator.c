#include <stdio.h>
#include <stdlib.h>

float getNumbers (float number)
{
    printf("Choose the numbers: ");
    scanf("%f", &number);
    return number;
}

int getOperation(int operation)
{
    printf("Hello! Let's calculate together!\nHere's the operations available:\n");
    printf("1)Add\n2)Subtract\n3)Multiply\n4)Divide\n");
    printf("Choose the operation: ");
    scanf("%d", &operation);
    return operation;
}

float makeCalc(float valorMain)
{
    return valorMain;
}

void showCalc(float finalValue)
{
    printf("Result: %.2f\n", finalValue);
}

int main(void)
{
    int operation = getOperation(operation);
    float x = getNumbers(x);
    float y = getNumbers(y);
    float result;

    switch ( operation )
    {

    case 1 :
        result = makeCalc(x + y);
        showCalc(result);
        break;

    case 2 :
        result = makeCalc(x - y);
        showCalc(result);
        break;

    case 3 :
        result = makeCalc(x * y);
        showCalc(result);
        break;

    case 4 :
        result = makeCalc(x / y);
        showCalc(result);
        break;

    default :
        printf("Something is wrong, please try again!\n");
        break;
    }

    return 0;
}
